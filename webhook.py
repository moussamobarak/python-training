import requests
# Provide the webhook URL that Discord generated
discord_webhook_url = 'https://discord.com/api/webhooks/793952542840717373/Ci1saqwvRV1M519AzxYPMf1IGN0qfB1hwfg0BbmdRijLKuEhhoEKfVzLjFUJAD2SD062'

# Get the BTC price from CoinDesk
bitcoin_price_url = 'https://api.coindesk.com/v1/bpi/currentprice/BTC.json'
data = requests.get(bitcoin_price_url).json()
price_in_usd = data['bpi']['USD']['rate']

# Post the message to the Discord webhook
data = {
    "content": "Bitcoin price is currently at $" + price_in_usd + " USD"
}
requests.post(discord_webhook_url, data=data)